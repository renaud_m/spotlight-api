'use strict'

const Database = use('Database')

const __types = use('App/Utils/types')
const __models = use('App/Utils/models')
const __placeholders = use('App/Utils/placeholders')

const User = use('App/Models/User')
const Attribute = use('App/Models/Attribute')
const AttributeLabel = use('App/Models/AttributeLabel')
const AttributeComputation = use('App/Models/AttributeComputation')
const AttributeReference = use('App/Models/AttributeReference')

const ReferenceValueItem = use('App/Models/ReferenceValueItem')

class AttributeController {
	async edit({params, request, response}) {
		try {
			let value = request.input('value')

			const result = await Database
				.select(params.pivotField)
				.from(params.pivotTable)
				.where('attribute_id', params.id)
				.update(params.pivotField, value)

			return response.json({
				query_name: 'editCol' + '|' + new Date,
				status: 'success',
				data: result
			})
		} catch(error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}

	async get({params, response}) {
		try {
			const getAllValues = () => {

				const result = Database
					.select('*')
					.from(params.table)
					.where('attribute_id', params.attrId)
				return result
			} 

			const getComputationData = () => {
				const result = Database
					.select(
						'attribute_computations.id',
						'attribute_computations.type',
						'attribute_computations.attribute_id'
					)
					.from('attribute_computations')
					.innerJoin('attributes', 'attributes.id', 'attribute_computations.attribute_id')
					.where('attribute_id', params.attrId)
				return result
			}

			async function getAll() {
				const promises = [getAllValues(), getComputationData()]
				const result = await Promise.all(promises)

				return result
			}

			const result = await getAll()

			return response.json({
				query_name: 'getCol' + '|' + new Date,
				status: 'success',
				data: result
			})
		} catch(error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}

	async getReference({params, response}) {
		try {
			const attributeReference = await AttributeReference.findBy('attribute_id', params.attrId)
	
			let selectedItems = []

			let referenceValueItemIds = await ReferenceValueItem.query().where('reference_value_id', params.valueId).ids()

			for (let i = 0; i < referenceValueItemIds.length; i++) {
				const referenceValueItemId = referenceValueItemIds[i]

				let referenceValueItem = await ReferenceValueItem.findBy('id', referenceValueItemId)

				if (referenceValueItem !== null) selectedItems.push(referenceValueItem)
				
			}


			let result = {
				attributeReference: attributeReference,
				selectedItems: selectedItems
			}

			return response.json({
				query_name: 'getCol' + '|' + new Date,
				status: 'success',
				data: result
			})
		} catch(error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}

	async getComputation({params, response}) {
		try {
			const result = await AttributeComputation.findBy('attribute_id', params.attrId)

			return response.json({
				query_name: 'getCol' + '|' + new Date,
				status: 'success',
				data: result
			})
		} catch(error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}

	async all({ params, response }) {
		try {
			const result = await Attribute.query().where('list_id', params.listId).orderBy('position').fetch()

			return response.json({
				query_name: 'getAttributes',
				status: 'success',
				data: result
			})
		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: 'Erreur lors de la récupération des attributs, (' + error.message + ')'
			})
		}
	}

	async add({ params, response }) {
		try {
			const lastAttribute = await Attribute.query().select('*').where('list_id', params.listId).max('position as position').first()
			
			const attribute = new Attribute()

			attribute.fill({
				list_id: params.listId,
				name: 'Attribut ' + (lastAttribute.position + 1),
				type: params.type,
				position: lastAttribute.position + 1
			})

			await attribute.save()

			const lastLine = await __models[lastAttribute.type].query()
				.select('*')
				.where('attribute_id', lastAttribute.id)
				.max('line as line').first()

			if (params.type === 'number') {
				const computation = new AttributeComputation()

				await computation.fill({
					'attribute_id': attribute.id,
					'type': 'sum'
				})

				await computation.save()
			}

			for (let i = 0; i < __types.length; i++) {
				const type = __types[i]

				if (type === params.type) {
					for (let i = 1; i <= lastLine.line; i++) {
						const line = new __models[type]()

						let base = {
							attribute_id: attribute.id,
							line: i
						}

						let insert = Object.assign(base, __placeholders[type])

						line.fill(insert)

						await line.save()
					}
				}
			}

			return response.json({
				query_name: 'add' + '|' + new Date + lastAttribute.id,
				status: 'success'
			})
		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}

	async delete({ params, response }) {
		try {
			const attribute = await Attribute.findBy('id', params.id)

			await Attribute.query()
				.where('list_id', params.listId)
				.where('position', '>', attribute.position)
				.decrement('position', 1)

			if (attribute.type === 'reference') {
				await AttributeReference.query().where('attribute_id', attribute.id).delete()
				
				const referenceValueIds = await __models.reference.query().where('attribute_id', attribute.id).ids()

				for (let i = 0; i < referenceValueIds.length; i++) {
					const referenceValueId = referenceValueIds[i]

					await ReferenceValueItem.query().where('reference_value_id', referenceValueId).delete()
				}

				await __models.reference.query().where('attribute_id', attribute.id).delete()


				const pivotAttributeReference = await AttributeReference.findBy('pivot_list_id', params.listId)
				const pivotAttribute = await Attribute.findBy('id', pivotAttributeReference.attribute_id)

				const pivotReferenceValueIds = await __models.reference.query().where('attribute_id', pivotAttribute.id).ids()

				for (let i = 0; i < pivotReferenceValueIds.length; i++) {
					const pivotReferenceValueId = pivotReferenceValueIds[i]

					await ReferenceValueItem.query().where('reference_value_id', pivotReferenceValueId).delete()
				}

				await __models.reference.query().where('attribute_id', pivotAttribute.id).delete()

				await pivotAttributeReference.delete()
				await pivotAttribute.delete()
			}

			for (let i = 0; i < __types.length; i++) {
				let type = __types[i]

				if (type === attribute.type) {
					await __models[type].query().where('attribute_id', attribute.id).delete()
				}
			}

			if (attribute.type === 'number') {
				await AttributeComputation.query().where('attribute_id', attribute.id).delete()
			}

			if (attribute.type === 'label') {
				await AttributeLabel.query().where('attribute_id', attribute.id).delete()
			}

			
			await attribute.delete()

			return response.json({
				query_name: 'delete' + '|' + new Date,
				status: 'success',
				data: attribute
			})
		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}

	async move({ params, response }) {
		try {
			let from = params.from
			let to = params.to

			let attribute = await Attribute.query().where('position', from).where('list_id', params.listId).first()
			attribute.merge({ position: to })
			
			await attribute.save()

			if (from > to) {
				await Attribute.query()
					.where('list_id', params.listId)
					.whereBetween('position', [to, from])
					.whereNot('id', attribute.id)
					.increment('position', 1)			
			} else {
				await Attribute.query()
					.where('list_id', params.listId)
					.whereBetween('position', [from, to])
					.whereNot('id', attribute.id)
					.decrement('position', 1)	
			}

			return response.json({
				query_name: 'updateAttrPosition' + '|' + new Date,
				status: 'success',
				data: attribute
			})
		} catch(error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}
}

module.exports = AttributeController