'use strict'

const Database = use('Database')

const User = use('App/Models/User')

const List = use('App/Models/List')

const Attribute = use('App/Models/Attribute')
const AttributeLabel = use('App/Models/AttributeLabel')
const AttributeComputation = use('App/Models/AttributeComputation')

const LabelValue = use('App/Models/LabelValue')
const TextValue = use('App/Models/TextValue')
const NumberValue = use('App/Models/NumberValue')
const DateValue = use('App/Models/DateValue')

const __types = ['text', 'number', 'date', 'label']

const __placeholders = {
	'text': {
		value: 'Ici un texte décrivant la cellule',
		color: 'black',
		important: 0
	},
	'number': {
		value: 0,
		color: 'black',
		important: 0
	},
	'date': '2019-12-10 12:26:22',
	'label': {
		value: '',
		color: 'grey'
	}
}

class ListController {
	async add({ request, auth, response }) {
		try {
			const list = new List()

			let user = await User.find(auth.current.user.id)
			let name = request.get('name')

			list.fill(name)

			const result = await list.save()

			await Database
				.insert({'user_id': user.id, 'list_id': list.id})
				.into('users_has_lists')

			const attribute = new Attribute()

			attribute.fill({
				list_id: list.id,
				name: 'Nom de la colonne',
				type: 'text',
				position: 1
			})

			await attribute.save()

			const textValue = new TextValue()

			textValue.fill({
				attribute_id: attribute.id,
				value: 'Ici un texte décrivant la cellule',
				line: 1,
				color: 'black',
				important: 0
			})

			await textValue.save()
			
			return response.json({
				query_name: 'add',
				status: 'success',
				data: list
			})

		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}

	async delete({ params, response }) {
		try {
			
			const id = params.id
			const list = await List.find(id)

			await list.delete()

			await Database
				.table('users_has_lists')
				.where('list_id', id)
				.delete()

			const attributeIds = await Attribute.query().where('list_id', id).ids()

			for (let i = 0; i < attributeIds.length; i++) {
				let attributeId = attributeIds[i]
				let attribute = await Attribute.query().where('id', attributeId).first()

				await attribute.delete()

				if (attribute.type === 'text') {
					await TextValue.query().where('attribute_id', attribute.id).delete()
				}

				if (attribute.type === 'number') {
					await NumberValue.query().where('attribute_id', attribute.id).delete()
					await AttributeComputation.query().where('attribute_id', attributeId).delete()
				}

				if (attribute.type === 'label') {
					await LabelValue.query().where('attribute_id', attribute.id).delete()
					await AttributeLabel.query().where('attribute_id', attributeId).delete()
				}

				if (attribute.type === 'date') {
					await DateValue.query().where('attribute_id', attribute.id).delete()
				}
			}
				
			return response.json({
				query_name: 'delete',
				status: 'success',
				data: list
			})

		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}

	async getAll({ auth, response }) {
		try {
			const user = await User.find(auth.current.user.id)

			const result = await Database
				.select('lists.id', 'lists.name', 'lists.description')
				.from('lists')
				.innerJoin('users_has_lists', 'lists.id', 'users_has_lists.list_id')
				.where('user_id', user.id)

			return response.json({
				query_name: 'getAll',
				status: 'success',
				data: result
			})

		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: 'Erreur lors de l\'affichage des listes, (' + error.message + ')'
			})
		}
	}

	async getItem({ params, response }) {
		try {
			const list = await Database
				.select('lists.id', 'lists.name', 'lists.description')
				.from('lists')
				.where('lists.id', params.id)

			return response.json({
				query_name: 'getItem',
				status: 'success',
				data: list
			})

		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: 'Erreur lors de l\'affichage de la liste, (' + error.message + ')'
			})
		}
	}

	async edit({ params, request, response }) {
		try {
			let value = request.input('value')

			const edittedList = await Database
				.select(params.target)
				.from(params.table)
				.where(params.table + '.id', params.id)
				.update(params.target, value)

			return response.json({
				query_name: 'edit' + '|' + new Date,
				status: 'success',
				data: value
			})
		}

		catch (error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}

	async editCol({params, request, response}) {
		try {
			let value = request.input('value')

			const result = await Database
				.select(params.pivotField)
				.from(params.pivotTable)
				.where('attribute_id', params.attrId)
				.update(params.pivotField, value)

			return response.json({
				query_name: 'editCol' + '|' + new Date,
				status: 'success',
				data: result
			})
		} catch(error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}

	async getCol({params, response}) {
		try {
			const getAllValues = () => {
				const result = Database
					.select('value', 'id', 'attribute_id')
					.from(params.table)
					.where('attribute_id', params.attrId)
				return result
			} 

			const getComputationData = () => {
				const result = Database
					.select(
						'attribute_computations.id',
						'attribute_computations.type',
						'attribute_computations.attribute_id'
					)
					.from('attribute_computations')
					.innerJoin('attributes', 'attributes.id', 'attribute_computations.attribute_id')
					.where('attribute_id', params.attrId)
				return result
			}

			async function getAll() {
				const promises = [getAllValues(), getComputationData()]
				const result = await Promise.all(promises)

				return result
			}

			const result = await getAll()

			return response.json({
				query_name: 'getCol' + '|' + new Date,
				status: 'success',
				data: result
			})
		} catch(error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}

	async getLine({params, response}) {
		try {
			const getValuesByType = (type) => {
				let table = type + '_values'

				let selection = [
					table + '.value',
					table + '.line',
					table + '.id',
					'attributes.name',
					'attributes.position',
					'attributes.type',
				]

				if (type === 'label' || type === 'number' || type === 'text') {
					selection.push(table + '.color')
				}

				return Database
					.select(selection)
					.from(table)
					.innerJoin('attributes', 'attributes.id', table + '.attribute_id')
					.where('attributes.list_id', params.listId)
					.where(table + '.line', params.line)
			}

			async function getAll() {
				const promises = []

				for (let i = 0; i < __types.length; i++) {
					const type = __types[i];

					promises.push(getValuesByType(type))
				}

				const data =  await Promise.all(promises)

				return [].concat(...data).sort((a, b) => a.position - b.position)
			}

			const result = await getAll()

			return response.json({
				query_name: 'getLine' + '|' + new Date,
				status: 'success',
				data: result
			})
		} catch(error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}

	async getAttributes({ params, response }) {
		try {
			const attributes = await Database
				.select('attributes.id', 'attributes.type', 'attributes.position', 'attributes.name')
				.from('attributes')
				.where('list_id', params.id)
				.orderBy('attributes.position')

			return response.json({
				query_name: 'getAttributes',
				status: 'success',
				data: attributes
			})
		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: 'Erreur lors de la récupération des attributs, (' + error.message + ')'
			})
		}
	}

	async getValues({ params, response }) {
		try {
			const getValuesByType = (valueType, options) => {
				const valueTable = valueType + '_values'

				const selectedValues = [
					valueTable + '.id',
					'attributes.type AS attribute_type',
					'attributes.id AS attribute_id',
					'attributes.name AS column_name',
					'attributes.position AS column',
					valueTable + '.line',
					valueTable + '.value'					
				]

				for (let type in options) {
					if (valueType === type) {
						let allOptions = options[type]

						for (let i = 0; i < allOptions.length; i++) {
							let option = allOptions[i]
							selectedValues.push(valueTable + '.' + option)
						}
					}
				}

				const promiseResult = Database
					.select(...selectedValues)
					.from(valueTable)
					.leftJoin('attributes', 'attributes.id', valueTable + '.attribute_id')
					.leftJoin('lists', 'lists.id', 'attributes.list_id')
					.where('lists.id', params.id)

				return promiseResult
			}

			const sliceArrayByLine = (arrayToSlice) => {
				let columns = 0,
					lines = 0,
					result = []

				for (let el of arrayToSlice) {
					if (el.column > columns) columns = el.column
					if (el.line > lines) lines = el.line
				}

				let sliceStartingIndex = columns * (lines - 1),
					counter = 1

				for (let i = 0; i <= sliceStartingIndex; i += columns) {
					let sliceEndingIndex = counter * columns

					let line = arrayToSlice.slice(i, sliceEndingIndex).sort(function (a, b) {
						return a.column - b.column
					})

					result.push(line)
					counter++
				}

				return result
			}

			async function getAllValues(types, additionalRows) {
				const promises = []

				for (let oneType of types) {
					promises.push(getValuesByType(oneType, additionalRows))
				}

				const allTypes = await Promise.all(promises)

				const data = [].concat(...allTypes).sort(function (a, b) {
					return a.line - b.line
				})

				return data.length > 0 ? sliceArrayByLine(data) : []
			}

			const result = await getAllValues(__types, {
				text: ['color', 'important'],
				number: ['color', 'important', 'unit'],
				label: ['color']
			})

			return response.json({
				query_name: 'getValues',
				status: 'success',
				data: result
			})
		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: 'Erreur lors de la récupération des valeurs, (' + error.message + ')'
			})
		}
	}

	async addCol({ params, response }) {
		try {
			const getAttrPosition = () => {
				const result = Database
					.max('position AS maxAttr')
					.from('attributes')
					.where('list_id', params.listId)

				return result
			}

			const getTotalLines = (type) => {
				let table = type + '_values'

				const result = Database
					.max('line AS totalLines')
					.from(table)
					.innerJoin('attributes', table + '.attribute_id', 'attributes.id')
					.where('attributes.list_id', params.listId)
					
				return result
			}

			const insertAttr = (position) => {
				const result = Database
					.insert({
						name: 'Nom de la colonne',
						type: params.type,
						position: position,
						list_id: params.listId
					})
					.into('attributes')

				return result
			}

			const insertComputation = (attrId, position) => {
				const result = Database
					.insert({
						type: 'sum',
						attribute_id: attrId
					})
					.into('attribute_computations')

				return result
			}

			const insertValues = (attrId, line) => {
				let insertions = {
					value: '',
					line: line,
					'attribute_id': attrId
				}

				for (let placeholder in __placeholders) {
					if (placeholder === params.type) {
						let placeholderValue = __placeholders[placeholder]

						if (typeof placeholderValue === 'object') {
							insertions = Object.assign(insertions, placeholderValue)
						} else {
							insertions.value = placeholderValue
						}
					}
				}

				const result = Database
					.insert(insertions)
					.into(params.type + '_values')

				return result
			}

			async function waitForMaxLine() {
				var maxLine = 0

				for (let type of __types) {
					const linesPromise = await new Promise ((res, rej) => {
						res(getTotalLines(type))
					})

					if (linesPromise[0].totalLines !== null) maxLine = linesPromise[0].totalLines
				}

				return maxLine
			}

			async function InsertNewCol() {
				const attrPositionPromise =  await new Promise( (res, rej) => {
					res(getAttrPosition())
				})

				let AttrPosition = attrPositionPromise[0].maxAttr + 1
				let totalLines = await waitForMaxLine()

				const newAttrId = await insertAttr(AttrPosition)

				const valuesPromises = (params.type === 'number') ?
				[insertComputation(newAttrId[0], AttrPosition)] : []

				for (let i = 0; i < totalLines; i++) {
					let line = i + 1
					valuesPromises.push(insertValues(newAttrId[0], line),)
				}

				const result = await Promise.all(valuesPromises)

				return result
			}

			const result = await InsertNewCol()

			return response.json({
				query_name: 'addCol' + '|' + new Date,
				status: 'success',
				data: result
			})
		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}

	async addLine ({params, response}) {
		try {
			const getAttrDatas = () => {
				const result = Database
					.select('id', 'name', 'type', 'position')
					.from('attributes')
					.where('list_id', params.listId)
					.orderBy('position', 'asc')

				return result
			}

			const getTotalLines = (type) => {
				let table = type + '_values'

				const result = Database
					.max('line AS totalLines')
					.from(table)
					.innerJoin('attributes', table + '.attribute_id', 'attributes.id')
					.where('attributes.list_id', params.listId)
					
				return result
			}

			const insertLineQuery = (type, line, attrId) => {
				let table = type + '_values'

				var insertions = {
					value: '',
					line: line,
					attribute_id: attrId
				}

				for (let placeholder in  __placeholders) {
					var value = __placeholders[placeholder]

					if (type === placeholder) {
						if (typeof value !== 'object') {
							insertions.value = value
						} else {
							for (let option in value) {
								if (option === 'value') {
									insertions.value = value.value
								} else {
									insertions = Object.assign({}, {
										[option]: value[option]
									}, insertions)
								}
							}
						}
					} 
				}

				const result = Database
					.insert(insertions)
					.into(table)

				return result
			}

			async function waitForMaxLine() {
				var maxLine = 0

				for (let type of __types) {
					const linesPromise = await new Promise ((res, rej) => {
						res(getTotalLines(type))
					})

					if (linesPromise[0].totalLines !== null) maxLine = linesPromise[0].totalLines
				}

				return maxLine
			}

			async function waitForLineInsertion(colDatas, lineMax) {

				for (let colData of colDatas) {
					const datasPromises = await new Promise((resolve, reject) => {
						resolve(insertLineQuery(colData.type, lineMax, colData.id))
					})
				}
			}

			async function insertAll() {
				const promises = [getAttrDatas(), waitForMaxLine()]

				const datasPromises = await Promise.all(promises)

				let attrDatas = datasPromises[0]
				let lineMax = datasPromises[1] + 1

				const result = await new Promise((resolve, reject) => {
					resolve(waitForLineInsertion(attrDatas, lineMax))
				})

				return result
			}

			const result = await insertAll()

			return response.json({
				query_name: 'addLine' + '|' + new Date,
				status: 'success',
				data: result
			})
		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}

	async deleteLine({params, response}) {
		try {
			const deleteValueByType = (type) => {
				let table = type + '_values'

				const result = Database
					.select(table + '.line', table + '.value', table + '.id', table + '.attribute_id')
					.from(table)
					.where('line', params.linePosition)
					.whereIn('attribute_id', function() {
						this.select('id')
							.from('attributes')
							.where('list_id', params.listId);
					})
					.del()

				return result
			}

			const updateLinesPosition = (type) => {
				let table = type + '_values'

				const result = Database
						.select('line', 'value')
						.from(table)
						.innerJoin('attributes', 'attributes.id', table + '.attribute_id')
						.where('line', '>', params.linePosition)
						.where('attributes.list_id', params.listId)
						.decrement('line', 1)

				return result
			}	

			async function getAllValues() {
				let result = []

				for (let type of __types) {
					const deletePromises = [deleteValueByType(type)]
					const resolveDelete = await Promise.all(deletePromises)

					result.push(resolveDelete)
				}

				for (let type of __types) {
					const updatePromises = [updateLinesPosition(type)]
					const resolveUpdate = await Promise.all(updatePromises)

					result.push(resolveUpdate)
				}

				return result
			}

			const result = await getAllValues()
			
			return response.json({
				query_name: 'deleteLine' + '|' + new Date,
				status: 'success',
				data: result
			})
		} catch(error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}

	async deleteCol({ params, response }) {
		try {
			const getPositionOfDeletedAttributes = () => {
				const result = Database
					.table('attributes')
					.where('attributes.id', params.attrId)

				return result
			}

			const updatePositionOfAllAttributes = (position) => {
				const result = Database
					.table('attributes')
					.where('attributes.list_id', params.listId)
					.where('position', '>', position)
					.decrement('position', 1)

				return result
			}

			const deleteAttribute = () => {
				const result = Database
					.table('attributes')
					.where('id', params.attrId)
					.delete()

				return result
			}

			const deleteComputation = () => {
				const result = Database
					.table('attribute_computations')
					.where('attribute_id', params.attrId)
					.delete()

				return result
			}

			const deleteLines = (type) => {
				let table = type + '_values'

				const result = Database
					.table(table)
					.where('attribute_id', params.attrId)
					.delete()

				return result
			}

			async function waitForGettingDeletedAttribute() {
				const result = await new Promise((resolve, reject) => {
					resolve(getPositionOfDeletedAttributes())
				})

				return result
			}

			async function waitForLinesDeletion() {
				for (let type of __types) {
					const lineDeletionPromise = await new Promise((resolve, reject) => {
						resolve(deleteLines(type))
					})
				}
			}

			async function deleteAll() {
				const gettingDeletedPromise = await new Promise((resolve, reject) => {
					resolve(waitForGettingDeletedAttribute())
				})

				const promises = [deleteAttribute(), deleteComputation(), waitForLinesDeletion()]

				await Promise.all(promises)

				const updateColumnsPosition = await new Promise((resolve, reject) => {
					resolve(updatePositionOfAllAttributes(gettingDeletedPromise[0].position))
				})

				const result = updateColumnsPosition

				return result
			}

			const result = await deleteAll()

			return response.json({
				query_name: 'deleteCol' + '|' + new Date,
				status: 'success',
				data: result
			})
		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}

	async search({ params, response }) {
		try {

			const result = await Database

			return response.json({
				query_name: 'search',
				status: 'success',
				data: result
			})
		} catch(error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}

	async getMembers({ params, response }) {
		try {
			const result = await Database

			return response.json({
				query_name: 'getMembers' + '|' + new Date,
				status: 'success',
				data: result
			})
		} catch(error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}

	async updateAttrPosition({ params, response }) {
		try {
			const selectOldItem = () => {
				const result = Database
					.select('attributes.id', 'attributes.position')
					.from('attributes')
					.innerJoin('lists', 'lists.id', 'attributes.list_id')
					.where('lists.id', params.listId)
					.where('attributes.position', params.oldPosition)
				return result
			}

			const update = (target, id) => {
				let result
				if (target === 'old') {
					result = Database
						.select('attributes.name', 'attributes.id', 'attributes.position')
						.from('attributes')
						.innerJoin('lists', 'lists.id', 'attributes.list_id')
						.where('lists.id', params.listId)
						.where('position', params.oldPosition)
						.update('position', params.newPosition)
				} else {
					result = Database
						.select('attributes.name', 'attributes.id', 'attributes.position')
						.from('attributes')
						.innerJoin('lists', 'lists.id', 'attributes.list_id')
						.where('lists.id', params.listId)
						.whereNot('attributes.id', id)

						if (params.oldPosition > params.newPosition) {
							result.whereBetween('position', [params.newPosition, params.oldPosition])
							result.increment('position', 1)
						} else {
							result.whereBetween('position', [params.oldPosition, params.newPosition])
							result.decrement('position', 1)
						}
				}
				
				return result
			}

			async function updateBoth() {
				const oldIdPromise = await new Promise((resolve, reject) => {
					resolve(selectOldItem())
				})

				const promises = [
					update('old'),
					update('others', oldIdPromise[0].id)
				]

				const result = await Promise.all(promises)

				return result
			}

			const result = await updateBoth()

			return response.json({
				query_name: 'updateAttrPosition' + '|' + new Date,
				status: 'success',
				data: result
			})
		} catch(error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}

	async updateLinePosition({ params, response }) {
		try {
			const attributeIds = await Attribute.query().where('list_id', params.listId).ids()

			let from = params.oldPosition
			let to = params.newPosition

			for (let i = 0; i < attributeIds.length; i++) {
				let attribute = await Attribute.query().where('id', attributeIds[i]).first()
				
				if (attribute.type === 'text') {
					let value = await TextValue.findBy('line', from)
					value.merge({ line: to })
					
					await value.save()

					if (from > to) {
						await TextValue.query()
							.whereBetween('line', [to, from])
							.whereNot('id', value.id)
							.increment('line', 1)			
					} else {
						await TextValue.query()
							.whereBetween('line', [from, to])
							.whereNot('id', value.id)
							.decrement('line', 1)	
					}
				}

				if (attribute.type === 'date') {
					let value = await DateValue.findBy('line', from)
					value.merge({ line: to })

					await value.save()

					if (from > to) {
						await DateValue.query()
							.whereBetween('line', [to, from])
							.whereNot('id', value.id)
							.increment('line', 1)			
					} else {
						await DateValue.query()
							.whereBetween('line', [from, to])
							.whereNot('id', value.id)
							.decrement('line', 1)	
					}
				}

				if (attribute.type === 'number') {
					let value = await NumberValue.findBy('line', from)
					value.merge({ line: to })

					await value.save()

					if (from > to) {
						await NumberValue.query()
							.whereBetween('line', [to, from])
							.whereNot('id', value.id)
							.increment('line', 1)			
					} else {
						await NumberValue.query()
							.whereBetween('line', [from, to])
							.whereNot('id', value.id)
							.decrement('line', 1)	
					}
				} 

				if (attribute.type === 'label') {
					const value = await LabelValue.findBy('line', from)
					value.merge({ line: to })

					await value.save()

					if (from > to) {
						await LabelValue.query()
							.whereBetween('line', [to, from])
							.whereNot('id', value.id)
							.increment('line', 1)			
					} else {
						await LabelValue.query()
							.whereBetween('line', [from, to])
							.whereNot('id', value.id)
							.decrement('line', 1)	
					}
				}
			}

			const result = attributeIds

			return response.json({
				query_name: 'updateLinePosition' + '|' + new Date,
				status: 'success',
				data: result
			})
		} catch(error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}
}

module.exports = ListController