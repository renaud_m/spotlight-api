# Note d'intention

**Nom du projet**
Spotlight

## Présentation

Mon projet de spécialisation consistera au développement d’une application de gestion de projet.
Cette application sera composé de listes complètement modulables de part son architecture.
L’utilisateur vas créer des listes dans lesquelles il ajoutera des colonnes (appelés aussi “**attributs**”).
Ces colonnes pourront avoirs un nom complètement personnalisé et une valeurs définie (entier, texte, date, étiquette, …).
Une fois toutes les colonnes définis, alors l’utilisateur pourra ajouter des valeurs à chacune de ces colonnes comme bon lui semble, le principe étant dans le futur de l’application, de pouvoir développer un système de modules qui donnera la possibilité à l’utilisateur d’obtenir différentes informations ou de faire certains calculs par croisement de données avec les différentes valeurs de la base de données.

Une vue spécifique sera disponible pour chaque lignes de la liste dans laquelle l’utilisateur pourra ajouter des tâches.

## Environnement technique

**React**
Afin d’isoler au mieux les différents “composants” de l’application et étant intéressé par la connaissance de ce framework pour mon projet professionnel, React est selon moi une technologie à utiliser pour ce projet

**Adonis**
Ce framework NODEJS pilotera le back-end de mon projet, l’utilisation de cette techno permettra une architecture “Fullstack JS” et embarquera ainsi tous les avantages de ce langage, à savoir la possibilité de développer de manière asynchrone.

## Benchmark
**Airtable**
Principal concurrent de mon projet (car très similaire) cette application permet également d’organiser des données sous la forme de différentes vues...
=> [https://airtable.com/](https://airtable.com/)

**Monday**
Similaire à **Airtable**, monday à plus vocation à être utilisée en tant que todoList.
Certains éléments ont pu être repris tel que la gestion de calcul pour les attributs de type “Nombre”

=> [https://monday.com/lang/fr/](https://monday.com/lang/fr/)