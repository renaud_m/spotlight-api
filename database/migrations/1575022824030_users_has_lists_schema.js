'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UsersHasListsSchema extends Schema {
  up () {
    this.create('users_has_lists', (table) => {
      table.increments()
      table.integer('user_id').unsigned().notNullable()
      table.integer('list_id').unsigned().notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('users_has_lists')
  }
}

module.exports = UsersHasListsSchema
