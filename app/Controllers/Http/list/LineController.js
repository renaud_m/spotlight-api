'use strict'

const Database = use('Database')

const __types = use('App/Utils/types')
const __models = use('App/Utils/models')
const __placeholders = use('App/Utils/placeholders')

const Attribute = use('App/Models/Attribute')
const AttributeLabel = use('App/Models/AttributeLabel')
const AttributeComputation = use('App/Models/AttributeComputation')

const ReferenceValueItem = use('App/Models/ReferenceValueItem')

class LineController {
	async get({params, response}) {
		try {
			const getValuesByType = (type) => {
				let table = type + '_values'

				let selection = [
					table + '.value',
					table + '.line',
					table + '.id',
					'attributes.name AS column_name',
					'attributes.id AS attributeId',
					'attributes.type AS attribute_type',
					'attributes.position',
					'attributes.type',
				]

				const options = {
					label: ['color'],
					number: ['color', 'unit'],
					text: ['color']
				}

				if (options[type] !== undefined) {
					let params = options[type]

					for (let i = 0; i < options[type].length; i++) {
						const option = options[type][i]

						selection.push(table + '.' + option)
					}
				}

				return Database
					.select(selection)
					.from(table)
					.innerJoin('attributes', 'attributes.id', table + '.attribute_id')
					.where('attributes.list_id', params.listId)
					.where(table + '.line', params.line)
			}

			async function getAll() {
				const promises = []

				for (let i = 0; i < __types.length; i++) {
					const type = __types[i];

					promises.push(getValuesByType(type))
				}

				const data =  await Promise.all(promises)

				return [].concat(...data).sort((a, b) => a.position - b.position)
			}

			const result = await getAll()

			return response.json({
				query_name: 'getLine' + '|' + new Date,
				status: 'success',
				data: result
			})
		} catch(error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}

	async getReferenceItems({ params, response }) {
		try {
			const referenceValue = await __models.reference.query().where('attribute_id', params.attrId).where('line', params.line).first()
			const referenceValueItem = await ReferenceValueItem.query().where('reference_value_id', referenceValue.id).fetch()

			return response.json({
				query_name: 'all' + '|' + new Date,
				status: 'success',
				data: referenceValueItem
			})
		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: 'Erreur lors de l\'affichage des listes, (' + error.message + ')'
			})
		}
    }

	async add ({params, response}) {
		try {
			let attributeIds = await Attribute.query().where('list_id', params.listId).ids()

			let result

			for (let i = 0; i < attributeIds.length; i++) {
				let attributeId = attributeIds[i];
				let attribute = await Attribute.query().where('id', attributeId).first()

				let lastLine

				for (let i = 0; i < __types.length; i++) {
					let type = __types[i]

					if (attribute.type === type) {
						lastLine = await __models[type].query().where('attribute_id', attribute.id).max('line as line').first()

						const value = new __models[type]()

						let base = {
							attribute_id: attribute.id,
							line: lastLine.line + 1
						}

						let insert = {}

						if (type === 'number') {
							let row = await __models['number'].query().where('attribute_id', attribute.id).where('line', lastLine.line).first()
							insert = Object.assign(base, { unit: row.unit })
						}

						insert = Object.assign(base, __placeholders[type])

						value.fill(insert)

						await value.save()

						result = value.id
					}
				}
			}

			return response.json({
				query_name: 'add' + '|' + new Date + result,
				status: 'success'
			})
		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}
    
    async move({ params, response }) {
		try {
			const attributeIds = await Attribute.query().where('list_id', params.listId).ids()

			let from = params.from
			let to = params.to

			for (let i = 0; i < attributeIds.length; i++) {
				let attribute = await Attribute.query().where('id', attributeIds[i]).first()

				for (let i = 0; i < __types.length; i++) {
					let type = __types[i]

					if (attribute.type === type) {
						let value = await __models[type].query().where('line', from).where('attribute_id', attribute.id).first()

						value.merge({ line: to })
						
						await value.save()

						if (from > to) {
							await __models[type].query()
								.whereBetween('line', [to, from])
								.whereNot('id', value.id)
								.increment('line', 1)
	
						} else {
							await __models[type].query()
								.whereBetween('line', [from, to])
								.whereNot('id', value.id)
								.decrement('line', 1)
						}
					}
				}
			}

			const result = attributeIds

			return response.json({
				query_name: 'updateLinePosition' + '|' + new Date,
				status: 'success',
				data: result
			})
		} catch(error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}

	async delete({params, response}) {
		try {
			let attributeIds = await Attribute.query().where('list_id', params.listId).ids()
			let id

			for (let i = 0; i < attributeIds.length; i++) {
				const attributeId = attributeIds[i]

				let attribute = await Attribute.query().where('id', attributeId).first()
				
				for (let i = 0; i < __types.length; i++) {
					let type = __types[i]

					if (attribute.type === type) {
						let value = await __models[type].query()
							.where('line', params.line)
							.where('attribute_id', attributeId).first()

						id = value.id

						await __models[type].query()
							.where('line', params.line)
							.where('attribute_id', attributeId).delete()

						await __models[type].query()
							.where('line', '>', value.line)
							.where('attribute_id', attributeId).decrement('line', 1)
					}
				}	
			}
			
			return response.json({
				query_name: 'deleteLine' + '|' + new Date + id,
				status: 'success'
			})
		} catch(error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}
}

module.exports = LineController