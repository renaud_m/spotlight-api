'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Attribute extends Model {
	textValues() {
		return this.hasMany('App/Models/TextValue')
	}

	IntValues() {
		return this.hasMany('App/Models/IntValue')
	}

	DateValues() {
		return this.hasMany('App/Models/DateValue')
	}

	LabelValues() {
		return this.hasMany('App/Models/LabelValue')
	}

	attributeLabels() {
		return this.hasMany('App/Models/AttributeLabels')
	}

	computations() {
		return this.hasMany('App/Models/AttributeComputation')
	}

	references() {
		return this.hasMany('App/Models/AttributeReference')
	}
}

module.exports = Attribute
