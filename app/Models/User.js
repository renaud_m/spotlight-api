'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Hash = use('Hash')
const List = use('App/Models/User')

class User extends Model {
	static boot () {
		super.boot()

		/**
		* A hook to hash the user password before saving
		* it to the database.
		*
		* Look at `app/Models/Hooks/User.js` file to
		* check the hashPassword method
		*/
		this.addHook('beforeCreate', 'UserHook.hashPassword')
	}

	lists() {
		return this.belongsToMany('App/Models/List', 'user_id').pivotTable('users_has_lists')
	}

	groups() {
		return this
			.belongsToMany('App/Models/Group', 'user_id', 'group_id')
			.pivotTable('users_has_groups')
	}

	static get table () {
		return 'users'
	}

	static get primaryKey () {
		return 'id'
	}
}

module.exports = User