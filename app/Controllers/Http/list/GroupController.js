'use strict'

const __types = use('App/Utils/types')
const __models = use('App/Utils/models')

const User = use('App/Models/User')

const List = use('App/Models/List')

const Attribute = use('App/Models/Attribute')
const AttributeLabel = use('App/Models/AttributeLabel')
const AttributeComputation = use('App/Models/AttributeComputation')
const AttributeReference = use('App/Models/AttributeReference')
const ReferenceValueItem = use('App/Models/ReferenceValueItem')

const Group = use('App/Models/Group')

class GroupController {
	async get({ params, response }) {
		try {
			const group = await Group.find(params.id)

			return response.json({
				query_name: 'get',
				status: 'success',
				data: group
			})

		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}

	async all({ auth, response }) {
		try {
			const userData = await User.find(auth.current.user.id)
			const user = await User.find(userData.id)

			const groups = await user.groups().orderBy('id', 'asc').fetch()

			return response.json({
				query_name: 'add',
				status: 'success',
				data: groups
			})

		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}

	async members({ params, response }) {
		try {
			const group = await Group.find(params.id)

			let membersCount = await group.users().orderBy('id', 'asc').getCount()
			let members = await group.users().orderBy('id', 'asc').fetch()

			const result = {
				membersCount: membersCount,
				members: members
			}

			return response.json({
				query_name: 'add',
				status: 'success',
				data: result
			})

		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}

	async add({ auth, response }) {
		try {
			const userData = await User.find(auth.current.user.id)
			const user = await User.find(userData.id)

			const groupsCount = await user.groups().getCount()

			let groupIndex = groupsCount + 1

			const group = await user.groups().create(
				{
					name: 'Groupe ' + groupIndex
				}
			)

			return response.json({
				query_name: 'add',
				status: 'success',
				data: group
			})

		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}

	async delete({ params, response }) {
		try {
			const groupId = params.id
			const group = await Group.find(groupId)
			const user = await group.users().where('group_id', groupId).first()
			const listIds = await group.lists().ids()

			await user.groups().where('group_id', groupId).delete()

			for (let i = 0; i < listIds.length; i++) {
				const listId = listIds[i]

				const list = await List.find(listId)
	
				await list.delete()
	
				const attributeIds = await Attribute.query().where('list_id', listId).ids()
	
				for (let i = 0; i < attributeIds.length; i++) {
					let attributeId = attributeIds[i]
					let attribute = await Attribute.query().where('id', attributeId).first()
	
					await attribute.delete()

					for (let i = 0; i < __types.length; i++) {
						const type = __types[i]

						await __models[type].query().where('attribute_id', attribute.id).delete()

						if (type === 'number') {
							await AttributeComputation.query().where('attribute_id', attributeId).delete()
						}

						if (type === 'label') {
							await AttributeLabel.query().where('attribute_id', attributeId).delete()
						}

						if (attribute.type === 'reference') {
							let pivotAttributeIds = await AttributeReference.query().where('pivot_list_id', params.id).ids()
		
							for (let i = 0; i < pivotAttributeIds.length; i++) {
								let pivotAttributeId = pivotAttributeIds[i]
		
								let pivotAttribute = await AttributeReference.find(pivotAttributeId)
		
								await Attribute.query().where('id', pivotAttribute.attribute_id).delete()
								
								let pivotReferenceValueIds = await ReferenceValue.query().where('attribute_id', pivotAttribute.attribute_id).ids()
			
								for (let i = 0; i < pivotReferenceValueIds.length; i++) {
									await ReferenceValueItem.query().where('reference_value_id', pivotReferenceValueIds[i]).delete()
								}
								
								await ReferenceValue.query().where('attribute_id', pivotAttribute.attribute_id).delete()
							}
		
							let referenceValueIds = await ReferenceValue.query().where('attribute_id', attribute.id).ids()
		
							for (let i = 0; i < referenceValueIds.length; i++) {
								await ReferenceValueItem.query().where('reference_value_id', referenceValueIds[i]).delete()
							}
		
							await ReferenceValue.query().where('attribute_id', attribute.id).delete()
		
							await AttributeReference.query().where('attribute_id', attribute.id).delete()
							await AttributeReference.query().where('pivot_list_id', params.id).delete()
						}
					}
				}
			}


			return response.json({
				query_name: 'delete' + params.id,
				status: 'success',
				data: group
			})

		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}
}

module.exports = GroupController