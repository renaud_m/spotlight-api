'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Group extends Model {
	lists() {
		return this.hasMany('App/Models/List')
	}

	users() {
		return this
			.belongsToMany('App/Models/User', 'group_id', 'user_id')
			.pivotTable('users_has_groups')
	}
}

module.exports = Group
