'use strict'

const List = use('App/Models/List')

class ListTemplateController {
	async projectsAndClients({request, auth, response}) {
	    // get currently authenticated user
	    const user = auth.current.user

	    // Save tweet to database
	    const List = await List.create({
	      name: 'Projets',
	      description: 'Description de la liste des projets'
	    })

	    // fetch tweet's relations
	    await tweet.loadMany(['user', 'favorites', 'replies'])

	    return response.json({
	      status: 'success',
	      message: 'Tweet posted!',
	      data: tweet
	    })
	}
}

module.exports = ListTemplateController
