'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DateValueSchema extends Schema {
  up () {
    this.create('date_values', (table) => {
      table.increments()
      table.integer('attribute_id').unsigned().notNullable()
      table.integer('line')
      table.date('value').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('date_values')
  }
}

module.exports = DateValueSchema
