const __models = {
	text: use('App/Models/TextValue'),
	label: use('App/Models/LabelValue'),
	number: use('App/Models/NumberValue'),
	date: use('App/Models/DateValue'),
    reference: use('App/Models/ReferenceValue'),
    percentage: use('App/Models/PercentageValue'),
    email: use('App/Models/EmailValue'),
    phone: use('App/Models/PhoneValue'),
    link: use('App/Models/LinkValue')
}

module.exports = __models