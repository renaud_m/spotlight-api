'use strict'

const Database = use('Database')

const __types = use('App/Utils/types')
const __models = use('App/Utils/models')
const __placeholders = use('App/Utils/placeholders')

const User = use('App/Models/User')

const Group = use('App/Models/Group')

const List = use('App/Models/List')

const Attribute = use('App/Models/Attribute')
const AttributeLabel = use('App/Models/AttributeLabel')
const AttributeComputation = use('App/Models/AttributeComputation')
const AttributeReference = use('App/Models/AttributeReference')

const LabelValue = use('App/Models/LabelValue')
const TextValue = use('App/Models/TextValue')
const NumberValue = use('App/Models/NumberValue')
const DateValue = use('App/Models/DateValue')
const ReferenceValue = use('App/Models/ReferenceValue')
const ReferenceValueItem = use('App/Models/ReferenceValueItem')

class ListController {
	async add({ params, request, response }) {
		try {
			const listCount = await List.query().where('group_id', params.groupId).getCount()

			let value = listCount + 1

			const list = new List()

			list.fill({
				group_id: params.groupId,
				name: request.input('name') === undefined ? 'Liste ' + value : request.input('name')
			})

			await list.save()

			const attribute = new Attribute()

			attribute.fill({
				list_id: list.id,
				name: 'Attribut 1',
				type: 'text',
				position: 1
			})

			await attribute.save()

			const textValue = new TextValue()

			textValue.fill({
				attribute_id: attribute.id,
				value: 'Ici un texte décrivant la cellule',
				line: 1,
				color: 'black',
				important: 0
			})

			await textValue.save()
			
			return response.json({
				query_name: 'add',
				status: 'success',
				data: list
			})

		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}

	async delete({ params, response }) {
		try {
			const id = params.id
			const list = await List.find(id)

			await list.delete()

			const attributeIds = await Attribute.query().where('list_id', id).ids()

			for (let i = 0; i < attributeIds.length; i++) {
				let attributeId = attributeIds[i]
				let attribute = await Attribute.query().where('id', attributeId).first()

				await attribute.delete()

				if (attribute.type === 'text') {
					await TextValue.query().where('attribute_id', attribute.id).delete()
				}

				if (attribute.type === 'number') {
					await NumberValue.query().where('attribute_id', attribute.id).delete()
					await AttributeComputation.query().where('attribute_id', attributeId).delete()
				}

				if (attribute.type === 'label') {
					await LabelValue.query().where('attribute_id', attribute.id).delete()
					await AttributeLabel.query().where('attribute_id', attributeId).delete()
				}

				if (attribute.type === 'date') {
					await DateValue.query().where('attribute_id', attribute.id).delete()
				}

				if (attribute.type === 'reference') {
					let pivotAttributeIds = await AttributeReference.query().where('pivot_list_id', params.id).ids()

					for (let i = 0; i < pivotAttributeIds.length; i++) {
						let pivotAttributeId = pivotAttributeIds[i]

						let pivotAttribute = await AttributeReference.find(pivotAttributeId)

						await Attribute.query().where('id', pivotAttribute.attribute_id).delete()
						
						let pivotReferenceValueIds = await ReferenceValue.query().where('attribute_id', pivotAttribute.attribute_id).ids()
	
						for (let i = 0; i < pivotReferenceValueIds.length; i++) {
							await ReferenceValueItem.query().where('reference_value_id', pivotReferenceValueIds[i]).delete()
						}
						
						await ReferenceValue.query().where('attribute_id', pivotAttribute.attribute_id).delete()
					}

					let referenceValueIds = await ReferenceValue.query().where('attribute_id', attribute.id).ids()

					for (let i = 0; i < referenceValueIds.length; i++) {
						await ReferenceValueItem.query().where('reference_value_id', referenceValueIds[i]).delete()
					}

					await ReferenceValue.query().where('attribute_id', attribute.id).delete()

					await AttributeReference.query().where('attribute_id', attribute.id).delete()
					await AttributeReference.query().where('pivot_list_id', params.id).delete()
				}
			}
				
			return response.json({
				query_name: 'delete',
				status: 'success',
				data: list
			})

		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}

	async all({ params, response }) {
		try {
			const group = await Group.find(params.groupId)

			const lists = (group !== null) ? await group.lists().orderBy('id', 'asc').fetch() : []

			return response.json({
				query_name: 'getAll',
				status: 'success',
				data: lists
			})

		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: 'Erreur lors de l\'affichage des listes, (' + error.message + ')'
			})
		}
	}

	async get({ params, response }) {
		try {
            const result = await List.findBy('id', params.id)

			return response.json({
				query_name: 'getItem',
				status: 'success',
				data: result
			})

		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: 'Erreur lors de l\'affichage de la liste, (' + error.message + ')'
			})
		}
	}

	async getAvailableForReference({ params, response }) {
		try {
			const currentList = await List.find(params.id)

			const attributesIds = await Attribute.query().where('list_id', params.id).ids()

			let excludeLists = []

			for (let i = 0; i < attributesIds.length; i++) {
				const attributesId = attributesIds[i]

				let attribute = await Attribute.query().where('id', attributesId).first()

				let attributeRef = await AttributeReference.query().where('attribute_id', attribute.id).first()

				if (attributeRef !== null) excludeLists.push(attributeRef)
			}

			const result = await Database
				.select('lists.id', 'lists.name', 'lists.description')
				.from('lists')
				.where('group_id', params.groupId)
				.whereNot('lists.id', currentList.id)
				.where(function() {
					for (let i = 0; i < excludeLists.length; i++) {
						this.whereNot('lists.id', excludeLists[i].pivot_list_id)
					}
				})

			return response.json({
				query_name: 'getItem',
				status: 'success',
				data: result
			})

		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: 'Erreur lors de l\'affichage de la liste, (' + error.message + ')'
			})
		}
	}

	async edit({ params, request, response }) {
		try {
			let value = request.input('value')

			const edittedList = await Database
				.select(params.target)
				.from(params.table)
				.where(params.table + '.id', params.id)
				.update(params.target, value)

			return response.json({
				query_name: 'edit' + '|' + new Date,
				status: 'success',
				data: value
			})
		}

		catch (error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}
}

module.exports = ListController