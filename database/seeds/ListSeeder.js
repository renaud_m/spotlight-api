'use strict'

/*
|--------------------------------------------------------------------------
| ListSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Database = use('Database')

class ListSeeder {
	async run () {
		const List = await Database.table('lists').insert([
			{
				name: 'Projet',
				Description: 'Description de la liste des projets'
			},
			{
				name: 'Clients',
				Description: 'Description de la liste des clients'
			}
		])
	}
}

module.exports = ListSeeder
