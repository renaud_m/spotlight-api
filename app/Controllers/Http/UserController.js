'use strict'

const UserUtils = use('App/Models/Utils/UserUtils');
const User = use('App/Models/User');

const utils = new UserUtils();

class UserController {
	async signup ({request, auth, response}) {
		const userData = request.only(['firstname', 'lastname', 'username', 'email', 'password']);

		try {
			const user = await User.create(userData)
			const token = await auth.generate(user)

			return response.json({
				status: 'success',
				data: token
			})
		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: 'Une erreur s\'est produite lors de l\'envoi du formualire, Veuillez réessayer. (' + error.message + ')'
			})
		}
	}

	async signin ({request, auth, response}) {
		try {
			const token = await auth.attempt(
				request.input('email'),
				request.input('password')
			)

			return response.json({
				status: 'success',
				data: token
			})
		} catch (error) {
			response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}

	async getDatas({auth, response}) {
		try {
			const result = await User.find(auth.current.user.id)

			return response.json({
				status: 'success',
				data: result
			})
		} catch (error) {
			response.status(400).json({
				status: 'error',
				message: 'Email / Mot de passe incorrects'
			})
		}
	}

	async search({request, response}) {
		try {
			const username = request.input('username')

			let exeptions = request.input('exeptions')

			if (exeptions !== undefined) exeptions = [JSON.parse(exeptions)]

			let result = []

			if (username !== '') {
				result = await User.query()
					.where('username', 'like', username + '%')
					.where(function() {
						if (exeptions !== undefined) {
							for (let i = 0; i < exeptions.length; i++) {
								const exeption = exeptions[i]
								this.whereNot('username', exeption.username)
							}
						}
					})
					.limit(10)
					.fetch()
			}

			return response.json({
				status: 'success',
				data: result
			})
		} catch (error) {
			response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}
}


module.exports = UserController
