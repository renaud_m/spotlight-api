'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const User = use('App/Models/User')

class List extends Model {
	users() {
		return this.belongsToMany('App/Models/User', 'list_id').pivotTable('users_has_lists')
	}

	attributes() {
		return this.hasMany('App/Models/Attribute')
	}
}

module.exports = List
