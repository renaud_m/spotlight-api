'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.increments()
      table.string('username', 64).notNullable().unique()
      table.string('email', 254).notNullable().unique()
      table.string('password', 64).notNullable()
      table.string('firstname', 64).nullable()
      table.string('lastname', 64).nullable()
      table.integer('age').nullable()
      table.string('bio', 256).nullable()
      table.text('profile_picture_url').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
