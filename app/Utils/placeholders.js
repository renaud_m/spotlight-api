const __placeholders = {
	'text': {
		value: 'Ici un texte décrivant la cellule',
		color: 'black',
		important: 0
	},
	'number': {
		value: 0,
		color: 'black',
		important: 0
	},
	'date': {
		value: '2019-12-10 12:26:22'
	},
	'label': {
		value: '',
		color: 'grey'
	},
	'percentage': {
		value: 0
	},
	'link': {
		value: ''
	},
	'phone': {
		value: ''
	},
	'email': {
		value: ''
	},
	'reference': {
		value: 0
	}
}

module.exports = __placeholders