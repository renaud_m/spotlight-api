'use strict'

const __types = use('App/Utils/types')
const __models = use('App/Utils/models')
const __placeholders = use('App/Utils/placeholders')

const List = use('App/Models/List')

const Attribute = use('App/Models/Attribute')
const AttributeReference = use('App/Models/AttributeReference')

const ReferenceValueItem = use('App/Models/ReferenceValueItem')

class ReferenceController {
	async all({ params, response }) {
		try {
			const attributeReference = await AttributeReference.query().where('attribute_id', params.attrId).first()
			const referenceValueItem = await ReferenceValueItem.query().where('reference_value_id', params.valueId).fetch()
			
			const result = {
				attributeReference: attributeReference,
				referenceValueItem: referenceValueItem
			}

			return response.json({
				query_name: 'all' + '|' + new Date,
				status: 'success',
				data: result
			})
		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: 'Erreur lors de l\'affichage des listes, (' + error.message + ')'
			})
		}
	}
	
	async add({ params, request, response }) {
		try {
			async function addByList(listId, pivotListId) {
				const lastAttribute = await Attribute.query().select('*').where('list_id', listId).max('position as position').first()

				const attribute = new Attribute()

				attribute.fill({
					list_id: lastAttribute.list_id,
					name: 'Attribut ' + (lastAttribute.position + 1),
					type: 'reference',
					position: lastAttribute.position + 1
				})

				const listName = await List.query().select('name').where('id', pivotListId).first()

				attribute.merge({name: listName.name})

				await attribute.save()

				const attributeReference = new AttributeReference()

				attributeReference.fill({
					attribute_id: attribute.id,
					pivot_list_id: pivotListId
				})

				await attributeReference.save()

				const lastLine = await __models[lastAttribute.type].query()
					.where('attribute_id', lastAttribute.id)
					.max('line as line')
					.first()

				for (let i = 1; i <= lastLine.line; i++) {
					const line = new __models.reference()

					let insert = {
						attribute_id: attribute.id,
						line: i,
						value: 0
					}

					line.fill(insert)

					await line.save()
				}

				return attributeReference
			}

			let pivotListId = request.input('pivotListId')

			let ref = await addByList(params.listId, pivotListId)
			let pivotRef = await addByList(pivotListId, params.listId)

			ref.pivot_attribute_id = pivotRef.attribute_id
			pivotRef.pivot_attribute_id = ref.attribute_id

			await ref.save()
			await pivotRef.save()

			return response.json({
				query_name: 'add' + '|' + new Date,
				status: 'success'
			})

		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: 'Erreur lors de l\'affichage des listes, (' + error.message + ')'
			})
		}
    }

    async addItem({ params, request, response }) {
		try {

			let referenceValueId = request.input('referenceValueId')
			let referenceLine = request.input('referenceLine')

			let currentLine = request.input('currentLine')
			let pivotListId = request.input('pivotListId')

			let currentListItem = new ReferenceValueItem()

			currentListItem.fill({
				reference_value_id: referenceValueId,
				reference_line: referenceLine
			})

			currentListItem.save()

			let attributeIds = await Attribute.query().where('list_id', pivotListId).where('type', 'reference').ids()

			if (attributeIds.length > 0) {
				let ref

				for (let i = 0; i < attributeIds.length; i++) {
					const attributeId = attributeIds[i]

					let buffer = await __models.reference.query().where('line', referenceLine).where('attribute_id', attributeId).first()

					if (buffer !== null) ref = buffer
				}

				const referenceValueItem = new ReferenceValueItem()

				referenceValueItem.fill({
					reference_value_id: ref.id,
					reference_line: currentLine
				})

				await referenceValueItem.save()
			}

			return response.json({
				query_name: 'add' + '|' + new Date,
				status: 'success'
			})

		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: 'Erreur lors de l\'affichage des listes, (' + error.message + ')'
			})
		}
    }

	async delete({ params, request, response }) {
		try {
			const referenceLine = request.input('referenceLine')
			const pivotListId = request.input('pivotListId')

			const referenceValueItem = await ReferenceValueItem.findBy('id', params.id)
			const referenceValue = await __models.reference.query().where('id', referenceValueItem.reference_value_id).first()
			const pivotLine = referenceValue.line
			
			await referenceValueItem.delete()

			const pivotAttributeIds = await Attribute.query().where('list_id', pivotListId).ids()
			let pivotAttributeReference

			for (let i = 0; i < pivotAttributeIds.length; i++) {
				let pivotAttributeId = pivotAttributeIds[i]

				let result = await AttributeReference.query().where('attribute_id', pivotAttributeId).first()

				if (result !== null) pivotAttributeReference = result
			}

			let pivotReferenceValueIds = await __models.reference.query().where('attribute_id', pivotAttributeReference.attribute_id).ids()

			let pivotLines = []

			for (let i = 0; i < pivotReferenceValueIds.length; i++) {
				const pivotReferenceValueId = pivotReferenceValueIds[i]
				let pivotReferenceValueItem = await ReferenceValueItem.query().where('reference_value_id', pivotReferenceValueId).first()

				if (pivotReferenceValueItem !== null) pivotLines.push(pivotReferenceValueItem.reference_line)
			}

			if (pivotLines.length > 1) {
				for (let i = 0; i < pivotReferenceValueIds.length; i++) {
					const pivotReferenceValueId = pivotReferenceValueIds[i]

					let referenceValue = await __models.reference.query().where('id', pivotReferenceValueId).where('line', referenceLine).first()

					if (referenceValue !== null) {
						let dels = await ReferenceValueItem.query().where('reference_value_id', referenceValue.id).ids()

						if (dels.length > 1) {
							let pivot = await __models.reference.query().where('id', referenceValueItem.reference_value_id).first()
							await ReferenceValueItem.query().where('reference_value_id', referenceValue.id).where('reference_line', pivot.line).delete()
						} else {
							await ReferenceValueItem.query().where('reference_value_id', referenceValue.id).delete()
						}
					}
				}
			} else {
				for (let i = 0; i < pivotReferenceValueIds.length; i++) {
					const pivotReferenceValueId = pivotReferenceValueIds[i]
					await ReferenceValueItem.query().where('reference_value_id', pivotReferenceValueId).where('reference_line', pivotLine).delete()
				}
			}

			return response.json({
				query_name: 'delete' + '|' + new Date,
				status: 'success'
			})

		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: 'Erreur lors de l\'affichage des listes, (' + error.message + ')'
			})
		}
    }
}

module.exports = ReferenceController