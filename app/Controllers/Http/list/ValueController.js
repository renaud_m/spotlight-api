'use strict'

const Database = use('Database')

const __types = use('App/Utils/types')
const __models = use('App/Utils/models')
const __placeholders = use('App/Utils/placeholders')


class ValueController {
    async all({ params, request, response }) {
		try {
			const exeptions = request.input('exeptions')

			const getValuesByType = (valueType, options) => {
				const valueTable = valueType + '_values'

				const selectedValues = [
					valueTable + '.id',
					'attributes.type AS attribute_type',
					'attributes.id AS attribute_id',
					'attributes.name AS column_name',
					'attributes.position AS column',
					valueTable + '.line',
					valueTable + '.value'					
				]

				for (let type in options) {
					if (valueType === type) {
						let allOptions = options[type]

						for (let i = 0; i < allOptions.length; i++) {
							let option = allOptions[i]
							selectedValues.push(valueTable + '.' + option)
						}
					}
				}

				let promiseResult

				if (exeptions) {

					let exeptionLines = []
				
					for (let i = 0; i < exeptions.length; i++) {
						const exeption = JSON.parse(exeptions[i])
						exeptionLines.push(exeption.reference_line)
					}

					promiseResult = Database
						.select(...selectedValues)
						.from(valueTable)
						.leftJoin('attributes', 'attributes.id', valueTable + '.attribute_id')
						.leftJoin('lists', 'lists.id', 'attributes.list_id')
						.where('lists.id', params.listId)
						.whereNotIn('line', exeptionLines)
				} else {
					promiseResult = Database
						.select(...selectedValues)
						.from(valueTable)
						.leftJoin('attributes', 'attributes.id', valueTable + '.attribute_id')
						.leftJoin('lists', 'lists.id', 'attributes.list_id')
						.where('lists.id', params.listId)
				}

				return promiseResult
			}

			const sliceArrayByLine = (arrayToSlice) => {
				let columns = 0,
					lines = 0,
					result = []

				for (let el of arrayToSlice) {
					if (el.column > columns) columns = el.column
					if (el.line > lines) lines = el.line
				}

				let sliceStartingIndex = columns * (lines - 1),
					counter = 1

				for (let i = 0; i <= sliceStartingIndex; i += columns) {
					let sliceEndingIndex = counter * columns

					let line = arrayToSlice.slice(i, sliceEndingIndex).sort(function (a, b) {
						return a.column - b.column
					})

					result.push(line)
					counter++
				}

				return result
			}

			async function getAllValues(types, additionalRows) {
				const promises = []

				for (let oneType of types) {
					promises.push(getValuesByType(oneType, additionalRows))
				}

				const allTypes = await Promise.all(promises)

				const data = [].concat(...allTypes).sort(function (a, b) {
					return a.line - b.line
				})

				return data.length > 0 ? sliceArrayByLine(data) : []
			}

			const result = await getAllValues(__types, {
				text: ['color', 'important'],
				number: ['color', 'important', 'unit'],
				label: ['color']
			})

			return response.json({
				query_name: 'getValues',
				status: 'success',
				data: result
			})
		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: 'Erreur lors de la récupération des valeurs, (' + error.message + ')'
			})
		}
	}

	async edit({ params, request, response }) {
		try {
			let value = request.input('value')

			const edittedList = await Database
				.select(params.target)
				.from(params.table)
				.where(params.table + '.id', params.id)
				.update(params.target, value)

			return response.json({
				query_name: 'edit' + '|' + new Date + value,
				status: 'success',
				data: value
			})
		}

		catch (error) {
			return response.status(400).json({
				status: 'error',
				message: error.message
			})
		}
	}
}

module.exports = ValueController