'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AttributeSchema extends Schema {
  up () {
    this.create('attributes', (table) => {
      table.increments()
      table.integer('list_id').unsigned().notNullable()
      table.string('type', 32).notNullable()
      table.integer('position')
      table.string('name', 64).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('attributes')
  }
}

module.exports = AttributeSchema