'use strict'

/*
|--------------------------------------------------------------------------
| AttributeSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Database = use('Database')

class AttributeSeeder {
	async run () {
		const attributes = await Database.table('attributes').insert([
			{
				list_id: 1,
				type: 'text',
				position: 1,
				name: 'Nom du Projet'
			},
			{
				list_id: 1,
				type: 'label',
				position: 2,
				name: 'Status'
			},
			{
				list_id: 2,
				type: 'text',
				position: 1,
				name: 'Nom du Client'
			},
			{
				list_id: 1,
				type: 'number',
				position: 3,
				name: 'Coût'
			},
			{
				list_id: 1,
				type: 'date',
				position: 4,
				name: 'Deadline'
			}
		])
	}
}

module.exports = AttributeSeeder
