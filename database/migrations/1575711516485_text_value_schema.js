'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TextValueSchema extends Schema {
  up () {
    this.create('text_values', (table) => {
      table.increments()
      table.integer('attribute_id').unsigned().notNullable()
      table.integer('line')
      table.string('value', 128).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('text_values')
  }
}

module.exports = TextValueSchema
