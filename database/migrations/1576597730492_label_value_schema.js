'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LabelValueSchema extends Schema {
  up () {
    this.create('label_values', (table) => {
      table.increments()
      table.integer('attribute_id').unsigned().notNullable()
      table.integer('line')
      table.string('value', 64).notNullable()
      table.string('color', 124).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('label_values')
  }
}

module.exports = LabelValueSchema
