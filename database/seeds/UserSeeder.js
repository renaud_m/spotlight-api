'use strict'

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Database = use('Database')

class UserSeeder {
	async run () {
		const UsersHasLists = await Database.table('users_has_lists').insert([
			{
				user_id: 1,
				list_id: 1
			},
			{
				user_id: 1,
				list_id: 2
			},
		])
	}
}

module.exports = UserSeeder
