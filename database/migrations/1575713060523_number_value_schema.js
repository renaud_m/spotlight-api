'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class NumberValueSchema extends Schema {
  up () {
    this.create('number_values', (table) => {
      table.increments()
      table.integer('attribute_id').unsigned().notNullable()
      table.integer('line')
      table.float('value', 16, 2).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('number_values')
  }
}

module.exports = NumberValueSchema
