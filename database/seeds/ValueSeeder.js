'use strict'

/*
|--------------------------------------------------------------------------
| ValueSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Database = use('Database')

class ValueSeeder {
  async run () {
		const textValues = await Database.table('text_values').insert([
			{
				attribute_id: 1,
				line: 1,
				value: 'Pilot Investment'
			},
			{
				attribute_id: 3,
				line: 1,
				value: 'Baptiste Guélé'
			},
			{
				attribute_id: 1,
				line: 2,
				value: 'Acad Express'
			},
			{
				attribute_id: 1,
				line: 3,
				value: 'Autocar Avenir Evasion'
			}
		])

		const numberValues = await Database.table('number_values').insert([
			{
				attribute_id: 4,
				line: 1,
				value: 1200
			},
			{
				attribute_id: 4,
				line: 2,
				value: 300
			},
			{
				attribute_id: 4,
				line: 3,
				value: 2250
			}
		])

		const dateValues = await Database.table('date_values').insert([
			{
				attribute_id: 5,
				line: 1,
				value: '2019-12-06'
			},
			{
				attribute_id: 5,
				line: 2,
				value: '2018-06-24'
			},
			{
				attribute_id: 5,
				line: 2,
				value: '2018-07-21'
			}
		])

		const labelValues = await Database.table('date_values').insert([
			{
				attribute_id: 2,
				line: 1,
				value: 'Terminé',
				color: 'green'
			},
			{
				attribute_id: 2,
				line: 2,
				value: 'Terminé',
				color: 'green'
			},
			{
				attribute_id: 2,
				line: 3,
				value: 'En cours',
				color: 'orange'
			}
		])
  }
}

module.exports = ValueSeeder
