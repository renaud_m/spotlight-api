'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')


// User Auth
Route.post('/signup', 'UserController.signup')
Route.post('/signin', 'UserController.signin')
Route.get('/user/search', 'UserController.search')

Route.group(() => {
	Route.get('/signin', 'FacebookController.redirect')
	Route.get('/callback', 'FacebookController.callback')
	Route.get('/logout', 'FacebookController.logout')
}).prefix('facebook')

// User
Route.group(() => {
	Route.get('/getDatas', 'UserController.getDatas')
}).prefix('user').middleware(['auth:jwt'])

Route.get('user/test', 'UserController.test')


/* ==== Group ==== */

Route.group(() => {
	Route.get('/get/:id', '/list/GroupController.get')
	Route.get('/:id/members', '/list/GroupController.members')
	Route.delete('/:id/delete', '/list/GroupController.delete')
}).prefix('group')

Route.group(() => {
	Route.get('/all', '/list/GroupController.all')
	Route.post('/add', '/list/GroupController.add')
}).prefix('group').middleware(['auth:jwt'])

/* ====  ==== */



/* ==== List ==== */

Route.group(() => {
	Route.get('/all/:groupId', '/list/ListController.all')
	Route.get('/get/:id', '/list/ListController.get')
	Route.get('/getAvailableForReference/:groupId/:id', '/list/ListController.getAvailableForReference')

	Route.post('/add/:groupId', '/list/ListController.add')
	Route.delete('/delete/:id', '/list/ListController.delete')
}).prefix('list')

/* ====  ==== */


/* ==== Attribute ==== */

Route.group(() => {
	Route.get('/all/:listId', '/list/AttributeController.all')
	Route.get('/get/:table/:attrId', '/list/AttributeController.get')
	Route.get('/getReference/:attrId/:valueId', '/list/AttributeController.getReference')
	Route.get('/getComputation/:attrId', '/list/AttributeController.getComputation')

	Route.post('/add/:listId/:type/', '/list/AttributeController.add')

	Route.put('/edit/:id/:pivotTable/:pivotField', '/list/AttributeController.edit')
	Route.put('/move/:from/:to/:listId', '/list/AttributeController.move')

	Route.delete('/delete/:id/:listId', '/list/AttributeController.delete')
}).prefix('list/attribute')

/* ====  ==== */


/* ==== Line ==== */

Route.group(() => {
	Route.get('/get/:line/:listId', '/list/LineController.get')
	Route.get('/getReferenceItems/:attrId/:line', '/list/LineController.getReferenceItems')

	Route.post('/add/:listId', '/list/LineController.add')

	Route.put('/edit/:line/:listId', '/list/LineController.edit')
	Route.put('/move/:from/:to/:listId', '/list/LineController.move')

	Route.delete('/delete/:line/:listId', '/list/LineController.delete')
}).prefix('list/line')

/* ====  ==== */


/* ==== Values ==== */

Route.group(() => {
	Route.get('/all/:listId', '/list/ValueController.all')
	Route.put('/edit/:table/:target/:id', '/list/ValueController.edit')
	Route.delete('/delete/:id', '/list/ValueController.delete')
}).prefix('list/value')

/* ====  ==== */


/* ==== Label ==== */

Route.group(() => {
	Route.get('/getAll/:attrId', '/list/types/LabelController.getAll')
	Route.post('/add', '/list/types/LabelController.add')
	Route.put('/edit/:id', '/list/types/LabelController.edit')
	Route.put('/edit/default/:id/:defaultColor/:defaultValue', '/list/types/LabelController.editDefault')
	Route.delete('/delete/:id', '/list/types/LabelController.delete')
}).prefix('label')

/* ====  ==== */

/* ==== Reference ==== */

Route.group(() => {
	Route.get('/getSelectedLines/:attrId/:valueId', '/list/types/ReferenceController.getSelectedLines')
	Route.get('/getAvailableLines/:attrId/:valueId', '/list/types/ReferenceController.all')
	Route.get('/all/:attrId/:valueId', '/list/types/ReferenceController.all')
	Route.post('/add', '/list/types/ReferenceController.add')
	Route.post('/addItem', '/list/types/ReferenceController.addItem')
	Route.delete('/delete/:id', '/list/types/ReferenceController.delete')
}).prefix('list/:listId/reference')

/* ====  ==== */