'use strict'

const Database = use('Database')

const Label = use('App/Models/LabelValue')
const AttributeLabel = use('App/Models/AttributeLabel')

class LabelController {
    async getAll({ params, response }) {
		try {
            const result = await Database
                .select('id', 'value', 'color')
                .from('attribute_labels')
                .where('attribute_id', params.attrId)

			return response.json({
				query_name: 'getAll' + '|' + new Date,
				status: 'success',
				data: result
			})

		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: 'Erreur lors de l\'affichage des listes, (' + error.message + ')'
			})
		}
    }
    
	async add({ request, response }) {
		try {
            let values = request.only(['attribute_id', 'color', 'value'])

            const result = await Database
                .insert(values)
                .into('attribute_labels')

			return response.json({
				query_name: 'add' + '|' + new Date,
				status: 'success',
				data: result
			})

		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: 'Erreur lors de l\'affichage des listes, (' + error.message + ')'
			})
		}
	}

	async delete({ params, response }) {
		try {
            const result = await Database
				.table('attribute_labels')
				.where('id', params.id)
				.delete()

			return response.json({
				query_name: 'delete' + '|' + new Date,
				status: 'success',
				data: result
			})

		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: 'Erreur lors de l\'affichage des listes, (' + error.message + ')'
			})
		}
	}

	async edit({ params, request, response }) {
		try {
			let data = request.only(['color', 'value'])

			const label = await Label.find(params.id)

			label.fill({
				id: label.id,
				color: data.color,
				value: data.value
			})

			await label.save()

			return response.json({
				query_name: 'edit' + '|' + new Date + '|' + params.id,
				status: 'success'
			})

		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: 'Erreur lors de l\'affichage des listes, (' + error.message + ')'
			})
		}
	}

	async editDefault({ params, request, response }) {
		try {
			let data = request.only(['color', 'value', 'targetId'])

			const attributeLabel = await AttributeLabel.find(params.id)

			const oldLabelIds = await Label.query()
			.where('attribute_id', attributeLabel.attribute_id)
			.where('color', params.defaultColor)
			.orWhere('value', params.defaultValue)
			.ids()

			attributeLabel.fill({
				id: attributeLabel.id,
				color: data.color,
				value: data.value
			})

			await attributeLabel.save()

			for (let i = 0; i < oldLabelIds.length; i++) {
				const oldLabel = await Label.find(oldLabelIds[i])

				oldLabel.fill({
					id: oldLabel.id,
					color: data.color,
					value: data.value
				})

				await oldLabel.save()
			}
			

			return response.json({
				query_name: 'edit' + '|' + new Date,
				status: 'success'
			})

		} catch (error) {
			return response.status(400).json({
				status: 'error',
				message: 'Erreur lors de l\'affichage des listes, (' + error.message + ')'
			})
		}
	}
}

module.exports = LabelController