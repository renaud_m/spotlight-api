const __types = [
    'text',
    'number',
    'date',
    'label',
    'percentage',
    'email',
    'phone',
    'link',
    'reference'
]

module.exports = __types